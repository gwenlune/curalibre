-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Mer 29 Mars 2017 à 13:09
-- Version du serveur :  10.1.22-MariaDB
-- Version de PHP :  7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `curalibre`
--

-- --------------------------------------------------------

--
-- Structure de la table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('836cf49512b27effd9d2b640e142c3d3a19d3a6d', '::1', 1487371969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373337313936393b),
('b7c5b8758ea92c0135fe073587d2d7bdca97d293', '::1', 1487372305, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373337323330353b),
('4df71cc11bf6682401ab7c742cca0275036b0fdd', '::1', 1487372645, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373337323634353b),
('4129aac7bbadc06c1d5db6e59cc5f863df2c498d', '::1', 1487375131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373337353133313b),
('e1f26eca5ff9b26128a1e4faa8c6f1bb7043fac3', '::1', 1487375324, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373337353133313b),
('6c4724955224005b2e826aa9cf42f42d439d69ca', '::1', 1487431680, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433313638303b),
('5ee215625ccaf58120981940fb05844c5ad6936c', '::1', 1487432633, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433323633333b),
('9d9bfd0c654473cf179ad94c0890341259260af2', '::1', 1487433017, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433333031373b),
('3b9e7fb4ecb8614b2a006bdde982c6490022abd3', '::1', 1487437832, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433373833323b),
('b77271dbd1fa6d4200e0a8aeadb261c7aeb6c7a5', '::1', 1487438903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433383930333b),
('ad438bfc3967d8e8195007ee67990103a0895d2d', '::1', 1487439223, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433393232333b),
('6095662d918008fcbd5e8aefae40d50cd41eecb3', '::1', 1487439224, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373433393232333b),
('0b8e82e6f49b562f3397728d5f43e89c5909dd74', '::1', 1487537117, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373533373131373b),
('31c3a913f6f81caa00959d184fab4aa2a4959b8e', '::1', 1487537572, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373533373537323b),
('7ed5bf8825909597a9c34f3f2abb3e9fdc42e480', '::1', 1487537973, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373533373937333b),
('85306bc9bfbdc3a421cc290848df5446ec29db27', '::1', 1487541824, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373534313832343b),
('8ae00a48fbbaea301c8e1b7a6df897496d07ebe4', '::1', 1487543251, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373534333235313b),
('78eaa394c9d6d6956f74f109a131b1f375b8bb02', '::1', 1487543701, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373534333730313b),
('d3cbb3a40fa0cba77df838758fdf1c6013d7f3e5', '::1', 1487543865, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373534333730313b),
('8d8118f927b1ea4de3a4bea90e0db3f69cc145e0', '::1', 1488620107, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383632303130373b),
('4fce00663a6c2e4e44eca6e093fd15aa51fc69f8', '::1', 1488620997, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383632303939373b),
('9fbfa28067686f33849610fb935f641b3bd6acef', '::1', 1488622168, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383632323136383b),
('b5a5583303dade78b019785ed710d607bf7440cc', '::1', 1488622198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383632323136383b),
('623e13f96332beb4988e4656c70e68dc7113ced4', '::1', 1488657859, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383635373835373b),
('0f91ead11f7f86f43d44e59e44c88af169e4ce4a', '::1', 1488830990, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383833303939303b),
('5b3937a6fe6fdb49acc17e8ff647a137d9a3c94d', '::1', 1488832750, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383833323735303b),
('9d47c0608e3d938b4b67dfd09bea243b53a38d67', '::1', 1488838398, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383833383339383b),
('1a050b3743990701e76bc17575da0dbc4447902e', '::1', 1488839865, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383833393836353b),
('86a0ecff4ec99bac223a8639aadc536fa1d46b06', '::1', 1488840309, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383834303330393b),
('4afcc1445fb11e82e90773a18045ef321ef3625b', '::1', 1488840885, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383834303838353b),
('6214aba77ff18942cbe823bcf500c2fd4a4db79e', '::1', 1488841402, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383834313430323b),
('66199a76254999bb4175007684e7a0fb3cb98c1c', '::1', 1488841689, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438383834313430323b),
('3f5c1aba4597fadc404ac272ba2ded70dece639d', '::1', 1489264932, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393236343933323b),
('dca5a24cb53593d30e289b7aecb6b934a294faee', '::1', 1489272793, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237323739333b),
('8d23d1ae78b3d00bf5505c75085aa6561677c8dc', '::1', 1489273117, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237333131373b),
('b3fb7acd09b4629f0126cf69ed23b16d6d282d9f', '::1', 1489273982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237333938323b),
('6f3ab9c47d3272261bed1da3f351c7ccbbc93b25', '::1', 1489274873, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237343837333b),
('f06c306981e82509d0d569ca19bbf771666a9a22', '::1', 1489275250, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237353235303b),
('64365a52c287c86e50be093204514bd95cdcc20e', '::1', 1489276232, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237363233323b),
('a4e572b617a397f7ac62bc46e4e1936c8978f7c4', '::1', 1489277820, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237373832303b),
('adf001e8df14dbce6191b518ad8bf9e4be9f7fb2', '::1', 1489277824, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393237373832303b),
('3eefb54ec236a847ac1b8f89a68d0096e33e4958', '::1', 1489352635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393335323633353b),
('9a4cff0caefca877e0709cc6cad1251a8c4320f8', '::1', 1489353328, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393335333332383b),
('482a87610b20a74d75656ec4504c7840bbb23bdc', '::1', 1489353339, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438393335333332383b),
('2ef4f9bdd01aee5c2ee06f60e7acd33aa7012024', '::1', 1490645804, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303634353739363b);

-- --------------------------------------------------------

--
-- Structure de la table `content`
--

CREATE TABLE `content` (
  `id` int(11) UNSIGNED NOT NULL,
  `source_id` int(11) UNSIGNED NOT NULL,
  `url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_preview` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_import` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL,
  `date_cure` timestamp NULL DEFAULT NULL,
  `vedette` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `content`
--

INSERT INTO `content` (`id`, `source_id`, `url`, `title`, `image_preview`, `content`, `date_import`, `archived`, `date_cure`, `vedette`) VALUES
(1, 1, 'http://keragora.tumblr.com/post/157400177575/conférence-participative-keragora-du-9-février', 'Conférence participative Keragora du 9 février 2017 « Drogues : dépénaliser pour mieux contrôler ? »', 'http://assets.tumblr.com/images/og/fb_landscape_share.png', 'Ci-dessous un compte rendu nécessairement non exhaustif des\néchanges de la soirée. Nous avons fait le choix de retenir les éléments de\ndébats qui nous ont paru refléter la teneur globale de la soirée, ainsi que les\npropos, commentaires, questionnements qui nous ont intéressés et/ou\ninterpellés.Invités\ntémoins :- Gildas ROUSSEL,\ndirecteur de l’Institut d’Etudes judiciaires de Brest', '2017-02-19 21:02:59', 0, NULL, NULL),
(2, 1, 'https://linuxfr.org/news/integration-de-owncloud-avec-onlyoffice', 'Intégration de ownCloud avec OnlyOffice', 'http://assets.tumblr.com/images/og/fb_landscape_share.png', 'Le 2 février 2017 est sortie l\'application « OnlyOffice pour ownCloud » qui permet aux utilisateurs d\'éditer des documents stockés sur ownCloud en utilisant les éditeurs de documents en ligne OnlyOffice.\nCette application « OnlyOffice pour ownCloud » est distribuée sous les termes de la licence GNU AGPL v3.\n\nOnlyOffice Document Server est une suite bureautique en ligne permettant une ', '2017-02-19 21:02:59', 0, NULL, NULL),
(3, 1, 'http://www.usine-digitale.fr/article/blockchain-transformer-ses-promesses-en-realite.N503294', 'Blockchain : transformer ses promesses en réalité', 'http://www.usine-digitale.fr/mediatheque/4/8/1/000520184_pageListeTypeACropped/francois-stephan.jpg', 'François Stephan, DGA de l’IRT SystemX (Institut de Recherche Technologique),  en charge du développement et de l’international © Gil Lefauconnier/IRT SystemX\n			\n										\n																\n							\n				\n				\n					&#13;\n	Pourquoi la blockchain défraie la chronique ? Parce qu’elle annonce une mutation profonde des tiers de confiance par l’adoption de la technologie numérique et l’app', '2017-02-19 21:02:59', 0, NULL, NULL),
(4, 1, 'http://www.frandroid.com/android/rom-custom-2/412155_kernel-et-android-quest-ce-que-cest-et-pourquoi-le-modifier', 'Kernel et Android : Qu’est-ce que c’est et pourquoi le modifier ? - FrAndroid', 'http://images.frandroid.com/wp-content/uploads/2017/02/android-kernel-explication.jpg', 'Vous êtes très nombreux à bidouiller votre appareil Android. Nous nous sommes intéressés au kernel, un composant essentiel de votre système : le kernel.\n\nAndroid offre d’énormes possibilités de personnalisation. Ce n’est pas le simple fait de l’ouverture du système, c’est également grâce aux choix technologiques de Google et au fonctionnement du système.\nVous êtes nombreux, am', '2017-02-19 21:02:59', 0, NULL, NULL),
(5, 1, 'http://www.journaldunet.com/economie/services/1191268-open-source-democratiser-intelligence-artificielle/', 'L\'open source veut démocratiser l\'intelligence artificielle', 'http://img-0.journaldunet.com/qElMO8rooDNa6BWYpWYul7FN9H4=/350x/smart/image-cms/10457095.jpg', 'Ces logiciels ouverts et gratuits peuvent être implémentés par de grands groupes mais aussi de petites entreprises qui veulent résoudre des problématiques business précises.        \n        Utiliser l\'intelligence artificielle pour répondre à des problématiques business est devenu un jeu d\'enfant pour les entreprises. Pour que même de petites PME au budget serré puissent bénéficier de', '2017-02-19 21:02:59', 0, NULL, NULL),
(6, 1, 'http://theconversation.com/relations-humaines-le-retour-69824', 'Relations humaines : le retour ?', 'http://cdn.theconversation.com/files/155206/width1356x668/image-20170201-29901-1yvssf3.jpg', 'Ce texte est tiré de la conférence « Au secours, les relations humaines reviennent ! » organisée par XERFI, la FNEGE, l’AGRH et l’ANDRH à Paris le 1ᵉʳ décembre 2016\n« Au secours, les relations humaines reviennent ! » C’est donc qu’elles étaient parties.\nIl est vrai que les relations humaines ne sont pas un problème pour des organisations qui attendent la performance de ', '2017-02-19 21:02:59', 0, NULL, NULL),
(7, 1, 'http://www.courrierinternational.com/article/vu-des-etats-unis-pourquoi-la-france-est-si-corrompue', 'Vu des États-Unis. Pourquoi la France est si corrompue', 'http://www.courrierinternational.com/sites/ci_master/files/styles/image_940/public/assets/images/fillonfrancoispenelopecorruption.jpg?itok=GoKgPtSR', '“Qui imagine le Général de Gaulle mis en examen ?” Cette phrase, lancée par François Fillon au mois d’août 2016 en référence à son rival Nicolas Sarkozy, a tendance à être souvent reprise par les médias depuis la révélation de l’affaire Penelope Fillon. Le magazine américain Foreign Policy ne fait pas exception, ajoutant dans la foulée :Maintenant que les enquêteurs fina', '2017-02-19 21:02:59', 0, NULL, NULL),
(8, 1, 'http://lvsl.fr/medias-ont-fabrique-candidat-macron', 'Comment les médias ont fabriqué le candidat Macron - Le Vent Se Lève', 'http://lvsl.fr/wp-content/uploads/2017/01/Macron-et-ses-unes-final.png', '&#13;\n																	 &#13;\n																																				&#13;\n																		&#13;\n																			&#13;\n																				&#13;\n																					&#13;\n																					&#13;\n																					 now viewing&#13;\n																				&#13;\n																			&#13;\n	&#13;\n																		&#13;\n																																				&#13;\n	&#13;\n																			&#13;\n				', '2017-02-19 21:02:59', 0, NULL, NULL),
(9, 1, 'http://eric-verhaeghe.entreprise.news/2017/02/02/en-route-pour-la-meme-chienlit-que-1958/', 'En route pour la même chienlit que 1958? - Jusqu\'ici, tout va bien...', 'http://eric-verhaeghe.entreprise.news/wp-content/uploads/sites/11/2017/02/Réception_du_Général_de_Gaulle_1958.jpg', 'Cet article a été lu 1157 fois Tout le monde se souvient de la chienlit de 1968. Mais il en est une, que l’on croyait conjurée, écartée, impossible à revoir: celle de 1958. En suivant les développements quasi-horaires maintenant de la campagne hostile à François Fillon, on se demande pourtant si le destin de la Vè République n’est pas de finir comme sa prédécesseuse. Plus d’un t', '2017-02-19 21:02:59', 0, NULL, NULL),
(10, 1, 'https://siecledigital.fr/2017/01/31/carte-du-monde-hyperloop/', 'Voici à quoi pourrait ressembler le monde connecté par Hyperloop.', 'https://sd-cdn.fr/wp-content/uploads/2017/01/hyperloop-carte-du-monde-300x211.jpeg', 'partages\n \n \n \n \n \n \n\nLa technologie de transport Hyperloop imaginée par Elon Musk semble conquérir le monde plus vite qu’il n’y paraît. Des projets naissent un peu partout sur Terre et un avenir où Hyperloop devient notre moyen de transport préféré se dessine. \nC’est à cette idée qu’un plan de métro international imaginé en 2003 par Mark Ovenden pourrait très bien s’applique', '2017-02-19 21:02:59', 0, NULL, NULL),
(11, 1, 'http://alternative21.blog.lemonde.fr/2017/01/30/revenu-universel-je-ne-comprends-pas-quon-ne-comprenne-pas/', 'Revenu universel: je ne comprends pas qu’on ne comprenne pas.', 'https://i1.wp.com/www.petitsvoiliers.com/wp-content/uploads/2016/07/bouee-de-sauvetage.jpg?resize=678%2C381', '&#13;\n        \n\n\n&#13;\n	\n\n\n&#13;\n	\n\n\n	&#13;\n\n\n\nDepuis que les médias se sont accaparés de « cette idée dont l’heure est venue », et que plusieurs candidats  à l’élection présidentielle  l’ont intégrée dans leur programme de gouvernement, je ne comprends pas  qu’on n’entende rien à ce revenu universel et inconditionnel. Je ne comprends pas qu’on ne comprenne pas que ce r', '2017-02-19 21:02:59', 0, NULL, NULL),
(12, 1, 'http://www.frandroid.com/culture-tech/406994_intelligence-artificielle-comment-fonctionne-un-cerveau-virtuel-cense-revolutionner-le-monde', 'Intelligence artificielle : comment fonctionne un cerveau virtuel censé révolutionner le monde ? - FrAndroid', 'http://images.frandroid.com/wp-content/uploads/2017/01/ia.jpg', 'L’intelligence artificielle est une notion dont en entend parler à longueur de journée et est annoncée comme la technologie qui va révolutionner nos usages. Mais il n’est pas aisé de comprendre le fonctionnement et les finalités de ces machines imitant nos cerveaux humains. Nous avons plongé dans les méandres de l’intelligence artificielle et de ses mécanismes pour vous expliquer cl', '2017-02-19 21:02:59', 0, NULL, NULL),
(13, 1, 'http://affordance.typepad.com//mon_weblog/2017/01/numerique-revenu-universel-espace-temps.html', 'Le numérique, le revenu universel et l\'espace-temps.', 'http://images.frandroid.com/wp-content/uploads/2017/01/ia.jpg', 'Dans le débat opposant Benoit Hamon à Manuel Valls on parle décidément beaucoup du désormais fameux \"revenu universel\" (utopie nécessaire), revenu universel dans le sillage duquel on parle également beaucoup de l\'impact du numérique sur l\'emploi avec une première (hypo)thèse indiquant qu\'il allait être destructeur d\'emplois. Le débat porte (entre autres) sur deux points.\nPrimo la fameu', '2017-02-19 21:02:59', 0, NULL, NULL),
(14, 1, 'https://framablog.org/2017/01/24/wallabag-it-une-belle-occasion-dy-aller-de-votre-poche/', 'Wallabag it, une belle occasion d’y aller de votre poche !', 'https://framablog.org/wp-content/uploads/2017/01/wallabagv2b2.png', 'Pour ne plus avoir 50 onglets ouverts « à lire plus tard » et garder tous les articles qu’on veut lire dans la poche, il existe une solution simple et libre : wallabag.\n\nWallabag, c’est le logiciel qui se trouve derrière notre service Framabag. Le principe est simple : vous vous inscrivez au service, et vous ajoutez l’extension à votre navigateur (Firefox ou Chrome/Chromium) afin d?', '2017-02-19 21:02:59', 0, NULL, NULL),
(15, 1, 'http://www.revenudebase.info/2017/01/24/mfrb-repond-rapport-de-lofce/', 'Le MFRB répond au rapport de l’OFCE – Mouvement Français pour un Revenu de Base', 'http://www.revenudebase.info/wp-content/uploads/2017/01/logo_ofce.png', 'Dans un rapport de l’Observatoire français des conjonctures économiques (OFCE) publié en décembre 2016[1], Guillaume Allègre et Henri Sterdyniak se sont penchés sur le sujet du revenu de base. Les auteurs soulèvent plusieurs questions liées à ce projet, évoquant les fondements de la mesure, ses avantages escomptés avant d’en pointer certaines limites. Le Mouvement Français pour un ', '2017-02-19 21:02:59', 0, NULL, NULL),
(16, 1, 'http://www.huffingtonpost.fr/malik-lounes/benoit-hamon-presidentielle-2017/', 'Pourquoi et comment Benoît Hamon peut gagner la primaire et la présidentielle', 'http://o.aolcdn.com/dims-shared/dims3/GLOB/crop/3941x2073+66+441/resize/1200x630!/format/jpg/quality/85/http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F5685c8ab08cc0074343f9a9b0c31c6ae%2F204846523', '\"Le Parti socialiste a organisé cette primaire pour régler un problème de ligne politique et ce problème n\'est pas du tout réglé. Tout le monde a compris qu\'ils ne sont d\'accord sur rien. Ils seront dans l\'incapacité de se réconcilier, de porter une dynamique, d\'incarner une alternative à l\'offre politique conservatrice de François Fillon et nationaliste de Marine Le Pen\". Et voilà comm', '2017-02-19 21:02:59', 0, NULL, NULL),
(17, 1, 'http://affordance.typepad.com//mon_weblog/2017/01/a-la-recherche-de-lutopie-.html', 'A la recherche de l\'utopie.', 'http://www.affordance.info/.a/6a00d8341c622e53ef01bb09703998970d-600wi', 'Au lendemain du premier tour des primaires du PS, journaux, radios, télés, éditorialistes de tous poils et de tous bords, opposants de droite et de gauche au projet de Benoît Hamon ont tous ce même mots à la bouche : \"utopie\". A tel point que même lui se sent dans l\'obligation de nier l\'utopie.\nCe projet, son projet, serait utopique. Wikipédia donne la définition suivante de l\'utopie : \n', '2017-02-19 21:02:59', 0, NULL, NULL),
(18, 1, 'http://eric-verhaeghe.entreprise.news/2017/01/23/faut-il-avoir-peur-du-revenu-universel/', 'Faut-il avoir peur du revenu universel? - Jusqu\'ici, tout va bien...', 'http://eric-verhaeghe.entreprise.news/wp-content/uploads/sites/11/2017/01/2423562782_5e06330d4e_b.jpg', 'Cet article a été lu 171 fois La probable victoire de Benoît Hamon dimanche prochain ouvrira un long débat sur le revenu universel, qui sera probablement un débat à front renversé. Cette idée d’essence libérale s’est en effet transformée, mois après mois (et sous l’influence de think tank auto-proclamés libéraux comme Générations Libres), en une doctrine socialisante sur laqu', '2017-02-19 21:02:59', 0, NULL, NULL),
(19, 1, 'http://tempsreel.nouvelobs.com/presidentielle-2017/20170119.OBS4055/primaire-le-ps-peut-il-survivre-pas-sur.html', 'Primaire : \"Le PS peut-il survivre ? Pas sûr…\"', 'http://referentiel.nouvelobs.com/file/15848841-le-ps-peut-il-survivre-pas-sur.jpg', 'Gérard Grunberg est directeur de recherche émérite au CNRS-Sciences Po. Spécialiste de la gauche, il dirige le centre de réflexion Telos.La primaire est le révélateur d\'un profond clivage qui divise la gauche. Ce clivage n\'est-il pas dû, au moins en partie, au fait que le gouvernement a changé de ligne politique et a quitté le point d\'équilibre qui permettait de rassembler les gauches ?', '2017-02-19 21:02:59', 0, NULL, NULL),
(20, 1, 'http://www.etopia.be/spip.php?article3123', 'Etopia | L’avenir du travail et de l’emploi, entre écologie et numérique', 'http://www.etopia.be/IMG/arton3123.jpg', 'Avant-propos\nCe texte est le point de départ d’une réflexion qui sera menée par Etopia à l’automne 2016, et soutenue par la Green European Foundation, autour de l’avenir du travail et de l’emploi. La présente contribution entend poser la problématique en esquissant une série de jalons afin de faire avancer la réflexion prospective des écologistes sur cette thématique décisive. C', '2017-02-19 21:02:59', 0, NULL, NULL),
(21, 1, 'http://authueil.fr/post/2017/01/22/Je-vote-Hamon', 'Je vote Hamon - Authueil', 'http://www.etopia.be/IMG/arton3123.jpg', 'J\'ai participé à la primaire socialiste. Je ne suis pas spécialement de\ngauche, mais l\'avenir du Parti socialiste, et plus largement de la gauche de\ngouvernement est une question qui intéresse tout le monde. Il est évident que\nles socialistes sont à la fin d\'un cycle et qu\'ils touchent le fond. Ils\npeuvent soit continuer à creuser, soit commencer à remonter. Alors que\nl\'extrême droite mon', '2017-02-19 21:02:59', 0, NULL, NULL),
(22, 1, 'http://www.numerama.com/politique/215539-robotique-travail-revenu-de-base-comment-benoit-hamon-reconcilie-modernite-et-social.html', 'Robotique, travail, revenu de base : comment Benoît Hamon réconcilie modernité et social - Politique - Numerama', 'http://www.numerama.com/content/uploads/2016/12/7175725181_d97b641a7d_k.jpg', 'Benoît Hamon était en décembre 2016 l\'invité de l\'Emission Politique de France 2. L\'occasion pour lui de montrer ses forces et ses faiblesses mais surtout d\'engager un débat de fond sur le travail. Mêlant réflexion philosophique et sociale, Hamon fait le portrait d\'un monde libéré du travail, mais juste. Une idée d\'avenir ? Nous sommes en mars 2016 lorsque nous interrogeons Benoît Hamon', '2017-02-19 21:02:59', 0, NULL, NULL),
(23, 1, 'http://fintech-mag.com/stephane-mallard-intelligence-artificielle/', 'Stéphane Mallard « En 2017, l’IA s’accélère »', 'http://fintech-mag.com/wp-content/uploads/2017/01/Stephane_mallard.jpg', 'Son job même est une disruption : « Digital Evangelist ». Ils sont nombreux à exercer ce métier aux Etats-Unis chez les géants du web et dans les startups, mais en France il est l’un des rares à assumer le terme connoté. Après avoir commencé sa carrière dans les salles de marché d’une banque, il a rejoint récemment Blu Age, une société qui automatise la transformation du code s', '2017-02-19 21:02:59', 0, NULL, NULL),
(24, 1, 'http://www.alternatives-economiques.fr/francois-chereque-un-honnete-homme/00076411', 'François Chérèque, un honnête homme', 'http://www.alternatives-economiques.fr/sites/default/files/public/styles/for_social_networks/public/thierry_pech_-_copie_0.jpg?itok=0b1BmitZ', '« Je n’ai pas eu le privilège de connaître personnellement François Chérèque, m’écrivait un ami il y a quelques jours, mais ce que je sais, c’est qu’avec sa disparition il y a un honnête homme de moins dans ce bas monde, alors que nous en avons si grand besoin ».L’homme que j’ai eu le privilège de connaître était en effet un honnête homme. Un homme droit, énergique et ', '2017-02-19 21:02:59', 0, NULL, NULL),
(25, 1, 'http://techtrends.eu/le-chomage-nest-pas-une-fatalite-cest-une-solution/', 'Le chômage n’est pas une fatalité, c’est une solution. | Techtrends', 'http://techtrends.eu/wp-content/uploads/2017/01/239ca47845469913-560x350.jpg', 'Vous avez peut-être lu dans la presse qu’au Japon, un robot va remplacer 34 salariés en assurance. \nPlus précisément, la société d’assurance-vie Fukoku Mutual Insurance va remplacer 25% de ses salariés du département des évaluations des paiements par un système d’intelligence artificielle (IA), l’IBM Watson Explorer. Un logiciel, donc. \nLe mot robot est souvent utilisé pour déc', '2017-02-19 21:02:59', 0, NULL, NULL),
(26, 1, 'http://www.numerama.com/sciences/225439-une-intelligence-artificielle-globale-et-agnostique-peut-elle-exister.html', 'Une intelligence artificielle globale et agnostique peut-elle exister ? - Sciences - Numerama', 'http://www.numerama.com/content/uploads/2017/01/ia-sentient-2.jpg', 'Aussi spectaculaires soient-ils, les récents progrès de l’intelligence artificielle ont été principalement le fait de logiciels ultra spécialisés, élaborés grâce à un travail de longue haleine. Mais l’intelligence artificielle de demain pourrait être, au contraire, polyvalente et autodidacte.« Nous vivons une époque à la fois fantastique et dangereuse. En effet, nous sommes tém', '2017-02-19 21:02:59', 0, NULL, NULL),
(27, 1, 'http://www.huffingtonpost.fr/valerie-foussier/adultes-surdoues-problemes-bureau/?ncid=fcbklnkfrhpmg00000001', 'Les 10 traits de caractères qui empoisonnent la vie des adultes surdoués', 'http://o.aolcdn.com/dims-shared/dims3/GLOB/crop/7360x3868+0+0/resize/1200x630!/format/jpg/quality/85/http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F4e7f013efe241d097354c6ca94ab0a56%2F204815417%2F', 'Les adultes surdoués ou à Haut potentiel (HP) ont un mode de fonctionnement bien différent et ignoré des autres adultes voire des HP eux-mêmes. Cette ignorance est à l\'origine de leur mal être créant un gouffre incompréhensif avec les autres, un décalage invivable. C\'est aussi leur mode d\'emploi qui fait leur force et qui est tant jalousé. Oui vous souffrez de vos capacités qui vous oc', '2017-02-19 21:02:59', 0, NULL, NULL),
(28, 1, 'http://www.parismatch.com/Actu/Societe/Une-enquete-qui-bouleverse-les-certitudes-Vie-apres-la-mort-890751', 'Vie après la mort - Une enquête qui bouleverse les certitudes', 'http://resize2-parismatch.ladmedia.fr/r/940,628/l/logo/w/N1Q4UnHE8qJki0cWg9IRSTpTQnZXNW5BK0RnMD0=/img/var/news/storage/images/paris-match/actu/societe/une-enquete-qui-bouleverse-les-certitudes-vie-apr', 'Paris Match. Avec votre “Test”, vous nous entraînez aux frontières du réel. Quelle est la genèse de cette aventure ?Stéphane Allix. La mort de mon frère, en 2001, dans un accident de voiture en Afghanistan, a bouleversé ma famille et imposé le sujet de la mort au cœur de notre existence. Depuis, je n’ai cessé de me poser des questions et j’ai complètement réorienté mon trava', '2017-02-19 21:02:59', 0, NULL, NULL),
(29, 1, 'http://lexpansion.lexpress.fr/actualite-economique/les-syndicats-ont-ils-trop-de-pouvoir_1865044.html', 'Les syndicats ont-ils trop de pouvoir?', 'http://static.lexpress.fr/medias_10862/w_1815,h_1362,c_crop,x_0,y_0/w_605,h_350,c_fill,g_north/v1457527672/manifestation-syndicats-loi-travail_5561373.jpg', 'De l\'élaboration d\'une partie du droit du travail à la gestion de pans entiers de notre protection sociale, syndicats et patronat disposent de larges prérogatives en France. Bien trop larges pour certains, au regard des blocages du système. Entre la n°2 de la CFDT et un ancien du Medef, aujourd\'hui très critique, le désaccord est total.\n  \n  \n          \n                    \n                ', '2017-02-19 21:02:59', 0, NULL, NULL),
(30, 1, 'http://pluzz.francetv.fr/videos/profs_en_territoires_perdus_de_la_republique_,151566668.html', 'Profs en territoires perdus de la République ? du 13-01-2017 à 00:25 en replay', 'http://static.lexpress.fr/medias_10862/w_1815,h_1362,c_crop,x_0,y_0/w_605,h_350,c_fill,g_north/v1457527672/manifestation-syndicats-loi-travail_5561373.jpg', 'Profs en territoires perdus de la République ? du 13-01-2017 à 00:25 en replay | Vidéo en streaming sur francetv pluzzDocumentaireSérie &amp; fictionMagazineCultureJeunesseDivertissementSportJeuRégionsEn savoir plus Voir aussi   En replayLes + regardésPublicité\n                    Encore +', '2017-02-19 21:02:59', 0, NULL, NULL),
(31, 1, 'http://www.lefigaro.fr/vox/politique/2017/01/09/31001-20170109ARTFIG00262-vers-la-quadripolarisation-de-la-vie-politique-francaise.php', 'Vers la quadripolarisation de la vie politique française', 'http://i.f1g.fr/media/figaro/orig/2017/01/09/XVMaebbdeec-d683-11e6-a484-5498a0c44dd8.jpg', 'Thomas Guénolé est politologue, maître de conférences à Sciences Po et docteur en Science politique (CEVIPOF). Il est l\'auteur de Les jeunes de banlieue mangent-ils les enfants? (éd. Le bord de l\'eau, 2015) et La mondialisation malheureuse (éd. First, 2016). FIGAROVOX. - Un sondage Ipsos-Sopra Steria pour Le Monde et le Cevipof annonce «Fillon-Le Pen-Macron-Mélenchon» comme quatuor d\'arr', '2017-02-19 21:02:59', 0, NULL, NULL),
(32, 1, 'http://m.nouvelobs.com/teleobs/polemique/20170103.OBS3332/le-courage-d-yves-calvi.html', 'Le courage d\'Yves Calvi', 'http://referentiel.nouvelobs.com/file/15820367-le-courage-d-yves-calvi.jpg', 'C\'est un paradoxe : quand un journaliste pose un acte de courage, on en parle dix fois moins que quand un autre \"dérape\" (comme on dit maintenant). La chose vient d\'arriver au détriment d\'Yves Calvi qui reprend désormais sur LCI, sous le titre \"24 Heures en questions\" (du lundi au vendredi, à 18h10), une émission comparable à son ancien \"C dans l\'air\" de France 5. De quel courage s\'agit-il ?', '2017-02-19 21:02:59', 0, NULL, NULL),
(33, 1, 'http://www.vousnousils.fr/2017/01/03/lecole-numerique-cest-se-livrer-aux-geants-de-linformatique-karine-mauvilly-597693', '« L’école numérique, c’est se livrer aux géants de l’informatique » (Karine Mauvilly) » VousNousIls', 'http://www.vousnousils.fr/wp-content/uploads/2017/01/couv-desastre-de-lecole-numerique2-300x182.jpg', 'Prof en collège, Karine Mauvilly a démissionné face au \"plan numérique pour l\'éducation\". Pour elle, il s\'agit d\'une intrusion du monde marchand à l\'école.\n							“Le désastre de l’école numérique” (Seuil)\n\nEx-journaliste, Karine Mauvilly a été prof d’histoire en collège pendant 2 ans. Face au développement du “tout-numérique” à l’école, elle a démissionné. Dans', '2017-02-19 21:02:59', 0, NULL, NULL),
(34, 1, 'http://edgard.fdn.fr/blog/index.php?post/2016/11/03/Amilitants', 'Amilitants - Oui, et alors ?', 'http://www.vousnousils.fr/wp-content/uploads/2017/01/couv-desastre-de-lecole-numerique2-300x182.jpg', 'Toute ma vie d\'adulteJe suis présent et actif dans diverses structures associatives, sur beaucoup de sujets, depuis 1994, en gros. Soit un peu plus de 20 ans. Certains engagements sont assez marqués, comportent un volet politique[], d\'autres le sont beaucoup moins (comme membre du TeX Users Group pendant des années, les implications politiques étaient faibles).\nMais tout ça fait que, toute ma', '2017-02-19 21:02:59', 0, NULL, NULL),
(35, 1, 'https://www.franceculture.fr/emissions/la-vie-numerique/debogage-dun-mythe-sur-le-numerique-lecole', 'Débogage d\'un mythe sur le numérique à l\'école', 'https://www.franceculture.fr/s3/cruiser-production/2017/01/bba3d1bc-ad51-45d6-9792-a3149401021a/600x337_thumb_eurythmie-3-waldorf.jpg', 'En poursuivant votre navigation sur le site, vous acceptez l’utilisation des cookies pour vous proposer des contenus et services adaptés à vos centres d’intérêt.\n            En savoir plus et gérer ces paramètres.', '2017-02-19 21:02:59', 0, NULL, NULL),
(36, 1, 'http://www.liberation.fr/elections-presidentielle-legislatives-2017/2017/01/05/benoit-hamon-le-revenu-universel-est-une-invitation-a-s-epanouir_1539421', 'Benoît Hamon : «Le revenu universel est une invitation à s’épanouir»', 'http://md1.libe.com/photo/980635-evpage2jpg.jpg?modified_at=1483682408&amp;picto=fb&amp;ratio_x=191&amp;ratio_y=100&amp;width=600', '«Je découvre que je vous intimide. Ça me touche beaucoup…» Premier candidat à accepter de répondre aux questions de la rédaction de Libération dans cette campagne de la primaire à gauche (22 et 29 janvier), Benoît Hamon attend sa première question après avoir feuilleté l’édition du journal de jeudi et expliqué vouloir faire de «la question des migrants» un débat «central?', '2017-02-19 21:02:59', 0, NULL, NULL),
(37, 1, 'http://www.lci.fr/replay/replay-l-invite-politique-du-6-janvier-2017-laurent-berger-2020416.html', 'REPLAY - L\'Invité Politique du 6 janvier 2017 : Laurent Berger', 'http://photos.lci.fr/images/720/404/laurent-berger-cfdt-lci-20170106-1100-a62c13-0@1x.png', 'Retrouvez sur LCI.fr le REPLAY - L’Invité Politique de François-Xavier Ménage le 6 janvier 2017 : Laurent Berger, secrétaire général de la CFDT.2017-01-06T08:35:35.000ZRetrouvez la question off issue de l\'interview politique de François-Xavier Ménage. Ce vendredi 6 janvier 2017, Laurent Berger, secrétaire général de la CFDT était son invité. Vous retrouverez le passage de l’émiss', '2017-02-19 21:02:59', 0, NULL, NULL),
(38, 1, 'http://www.lejdd.fr/Politique/Les-cinq-raisons-qui-expliquent-la-progression-de-Benoit-Hamon-837505', 'Les cinq raisons qui expliquent la progression de Benoît Hamon - leJDD.fr', 'http://cdn-lejdd.ladmedia.fr/var/lejdd/storage/images/media/images/politique/benoit-hamon-a-progresse-de-11-points-dans-les-sondages/13140955-1-fre-FR/Benoit-Hamon-a-progresse-de-11-points-dans-les-so', 'Benoît Hamon a progressé de 11 points dans les sondages. (Reuters)Le 8 décembre dernier, Benoit Hamon remplace, au pied levé, Manuel Valls et François Hollande, qui ont tous deux décliné l\'invitation de France 2 pour participer à L\'Emission politique. L\'ancien ministre de l\'Education nationale est prévenu seulement 72 heures avant. Il s\'enferme, bûche, prépare des notes. Le résultat, e', '2017-02-19 21:02:59', 0, NULL, NULL),
(39, 1, 'http://www.capital.fr/a-la-une/actualites/mort-de-francois-chereque-les-facettes-meconnues-de-l-ancien-patron-de-la-cfdt-1196770', 'Mort de François Chérèque : les facettes méconnues de l\'ancien patron de la CFDT', 'http://www.capital.fr/var/cap/storage/images/media/images/photos/rea/francois-chereque-c-laurent-cerino-rea/17283880-1-fre-FR/francois-chereque-c-laurent-cerino-rea.jpg', 'Son enfance virile Dans la fratrie Chérèque, François était le quatrième de cinq garçons. A Pompey (Meurthe-et-Moselle), où la tribu a grandi, le père, Jacques Chérèque, métallo à bacchantes et futur numéro 2 de la CFDT, n\'était pas souvent à la maison. C\'est donc la mère, Elisabeth, qui a élevé les cinq enfants nés en l\'espace de six ans.Sa passion de la mêlée Il a 12 ans qua', '2017-02-19 21:02:59', 0, NULL, NULL),
(40, 1, 'http://www.numerama.com/politique/220443-les-5-propositions-phares-darnaud-montebourg-pour-le-numerique.html', 'Primaire de la gauche : ce que proposent les candidats pour le numérique et la tech - Politique - Numerama', 'http://www.numerama.com/content/uploads/2016/12/arnaud-montebourg.jpg', 'Quelles sont les propositions des 7 candidats de la primaire de la gauche en matière de numérique, de tech et de nouvelle économie ? Retrouvez-les ici, actualisées au fil de la campagne, en attendant le premier tour du scrutin, le 22 janvier 2017.Depuis le 17 décembre 2016 et jusqu’au 20 janvier 2017 à minuit, les sept candidats de « la Belle alliance populaire » mènent campagne pou', '2017-02-19 21:02:59', 0, NULL, NULL),
(41, 1, 'http://www.sciencesetavenir.fr/high-tech/le-transhumanisme-est-un-neodarwinisme-dangereux-avertit-bernard-stiegler_108864', 'Ce philosophe dénonce les dangers du transhumanisme', 'http://www.sciencesetavenir.fr/assets/img/2016/12/13/cover-r4x3w1000-584ff5ef363e2-SIPA_00596717_000007.jpg', 'TRANSHUMANISME. Comment l\'évolution technologique transforme-t-elle la société ? Chaque année, les \"Entretiens du nouveau monde industriel\" convient philosophes et experts à débattre de cette question. Au programme de l\'édition 2016, qui se tenait mardi 13 et mercredi 14 décembre 2016 à Paris : les dangers du transhumanisme, cette doctrine issue de la Silicon valley qui vise à \"au', '2017-02-19 21:02:59', 0, NULL, NULL),
(42, 1, 'http://affordance.typepad.com//mon_weblog/2016/12/dystopie-2017.html', 'Dystopie 2017', 'http://www.affordance.info/.a/6a00d8341c622e53ef01b8d2457447970c-600wi', 'Quelques infos aperçues sur les internets qui, une fois rassemblées, m\'ont glacé les sangs. Ben oui.\nBig Brother à crédit\nEn Chine, le gouvernement souhaite appliquer à l\'ensemble de la population un système de \"notation\" et de \"points\" (Credit Score). J\'ai d\'abord cru à une info du Gorafi mais le Wall Street Journal indique que :\n&#13;\n\"Beijing veut attribuer à chaque citoyen un score ca', '2017-02-19 21:02:59', 0, NULL, NULL),
(43, 1, 'https://www.franceculture.fr/emissions/linvite-des-matins/uberisation-de-leconomie-la-fin-des-illusions', 'Uberisation de l\'économie : la fin des illusions ?', 'https://s3-eu-west-1.amazonaws.com/cruiser-production/2016/12/3b49140c-e301-4d3d-a6d8-d5bd5d2b6ecc/600x337_000_9885z.jpg', 'En poursuivant votre navigation sur le site, vous acceptez l’utilisation des cookies pour vous proposer des contenus et services adaptés à vos centres d’intérêt.\nEn savoir plus et gérer ces paramètres.', '2017-02-19 21:02:59', 0, NULL, NULL),
(44, 1, 'http://www.frandroid.com/comment-faire/tutoriaux/393591_comment-utiliser-le-gestionnaire-de-mot-de-passe-keepass-password-safe-tutoriel', 'Comment utiliser le gestionnaire de mot de passe KeePass Password Safe ? - Tutoriel - FrAndroid', 'http://images.frandroid.com/wp-content/uploads/2016/12/mr-robot-security-keepass-e1482516495485.jpg', 'KeePass Password Safe est un gestionnaire de mots de passe open source. Il permet d’avoir accès à tous ses mots de passe en n’en retenant qu’un seul. Cependant, sa mise en place peut faire peur, nous vous proposons donc un guide pour vous aider à réellement vous y mettre, que vous soyez sur Windows, macOS, Linux ou Android.\n\nOn nous le répète sans cesse, il faut utiliser un mot de pas', '2017-02-19 21:02:59', 0, NULL, NULL),
(45, 1, 'http://rue89.nouvelobs.com/2016/12/23/les-49-secrets-les-mieux-gardes-tech-2016-265965&_escaped_fragment_=', 'Les 49 secrets les mieux gardés de la tech en 2016 - Rue89 - L\'Obs', 'http://api.rue89.nouvelobs.com/sites/news/files/styles/mobile2-tablette-asset-center/public/assets/image/2016/12/lefutur.jpeg', '2016 n’aura pas été l’année de la réalité virtuelle, comme beaucoup de gamers l’escomptaient. La technologie est peut-être suffisamment au point pour les pilotes de la Drone Racing League, mais on aura surtout beaucoup ri en regardant les testeurs lutter pour évoluer dans les mondes virtuels interactifs proposés actuellement. \nMaking of\nToutes les histoires présentées dans cet ', '2017-02-19 21:02:59', 0, NULL, NULL),
(46, 1, 'http://www.lemonde.fr/idees/article/2016/12/22/comment-le-numerique-change-la-politique_5053079_3232.html', 'Comment le numérique change la politique', 'http://s1.lemde.fr/image/2016/12/22/644x322/5053078_3_f697_la-troisieme-revolution-industrielle-du-nu_cd258c76b6ab369e870d8c13a3294fd3.jpg', 'Le numérique nous fait entrer dans un nouvel âge du politique, avec les « civic tech », ces technologies qui doivent permettre aux citoyens de prendre part à la prise de décision publique et de se mobiliser pour l’intérêt général. Puissant facteur de désintermédiation, elles modifient le jeu des acteurs, à la fois sur le plan de la communication (court-circuitant les médias), de ', '2017-02-19 21:02:59', 0, NULL, NULL),
(47, 1, 'https://www.franceculture.fr/numerique/lawrence-lessig-la-segmentation-du-monde-que-provoque-internet-est-devastatrice-pour-la', 'Lawrence Lessig : \"La segmentation du monde que provoque Internet est  dévastatrice pour la démocratie\"', 'https://s3-eu-west-1.amazonaws.com/cruiser-production/2016/12/71aa2fd4-9a78-4d21-8352-59cd0db1274b/600x337_lessig_chip_somodevilla_getty_images_north_america_afp.jpg', 'Lawrence Lessig, professeur de droit à Harvard, penseur d\'Internet et initiateur de la licence Creative commons•\nCrédits : Chip Somodevilla \n-\nAFPC\'est avec une certaine inquiétude que Lawrence Lessig observe comment Internet est à la fois un outil fantastique et ce qui a renforcé la crise démocratique. Professeur de droit à Harvard, constitutionnaliste réputé, Lawrence Lessig est l\'un ', '2017-02-19 21:02:59', 0, NULL, NULL),
(48, 1, 'http://www.slate.fr/story/132347/lagarde-dispensee-peine-elites-coupables', 'Si Lagarde a été dispensée de peine, ce n\'est pas pour protéger les élites coupables', 'http://www.slate.fr/sites/default/files/styles/1090x500/public/lagarde.jpg', 'Simplement, le dossier de l\'accusation était vide.\n      \n      Un bon jugement est un jugement clair, droit et compréhensible par tous. La sentence émise par la cour de justice de la République (CJR) contre Christine Lagarde, jugée «coupable de négligence» mais dispensée de peine, est son exact contraire, le procès a été tout du long bancal, le résultat est de la même facture, cont', '2017-02-19 21:02:59', 0, NULL, NULL),
(49, 1, 'https://cyrille-borne.com/blog/index.php?article2699/tout-n-est-pas-si-pourri-quand-meme-en-fait-si-mais-on-va-faire-comme-si-c-etait-pas-vrai', 'No title found', 'http://www.slate.fr/sites/default/files/styles/1090x500/public/lagarde.jpg', 'wallabag can\'t retrieve contents for this article. Please report this issue to us.', '2017-02-19 21:02:59', 0, NULL, NULL),
(50, 1, 'http://www.numerama.com/content/uploads/2016/10/zuckerberg-vr-room.jpg', 'Image', 'http://www.slate.fr/sites/default/files/styles/1090x500/public/lagarde.jpg', '', '2017-02-19 21:02:59', 0, NULL, NULL),
(51, 1, 'http://www.numerama.com/content/uploads/2016/12/15419743_10103347287954901_2744013366467623932_o.jpg', 'Image', 'http://www.slate.fr/sites/default/files/styles/1090x500/public/lagarde.jpg', '', '2017-02-19 21:02:59', 0, NULL, NULL),
(52, 1, 'http://www.usine-digitale.fr/article/le-numerique-a-completement-revolutionne-le-pouvoir-faire-explique-mondher-abdennadher-fondateur-des-napoleons.N479259', '\"Le numérique a complètement révolutionné le pouvoir faire\", explique Mondher Abdennadher, fondateur des Napoléons', 'http://www.usine-digitale.fr/mediatheque/0/0/4/000343400_pageListeTypeACropped/salles-de-reunion-design.png', 'Le pouvoir est aussi une question d\'occupation de l\'espace. \n			\n										\n																									\n				\n				\n				\n					&#13;\n	L\'Usine Digitale : Après une session consacrée à la simplicité, les Napoléons reviennent en janvier avec le thème du pouvoir. Pourquoi l’avoir choisi ?&#13;\n&#13;\n	Mondher Abdennadher: Notre prisme, c’est l’innovation, et il nous a semblé qu’il se passe de', '2017-02-19 21:02:59', 0, NULL, NULL),
(53, 1, 'http://www.usine-digitale.fr/article/la-blockchain-moteur-de-l-economie-collaborative.N480074', 'La Blockchain, moteur de l\'économie collaborative', 'http://www.usine-digitale.fr/mediatheque/4/4/0/000504044_pageListeTypeACropped/eric-cohen.jpg', 'Eric Cohen, président fondateur du Groupe Keyrus © Keyrus\n			\n										\n																									\n				\n				\n				\n					&#13;\n	De la persistance de l\'information à la confiance numérique&#13;\n&#13;\n	Apparue en 2008 avec le Bitcoin, la technologie Blockchain promet de révolutionner les pratiques numériques en installant une relation de confiance sur laquelle l\'ensemble des transactions électr', '2017-02-19 21:02:59', 0, NULL, NULL),
(54, 1, 'http://www.numerama.com/politique/218121-presidentielle-2017-voxe-veut-reconcilier-la-jeunesse-et-la-politique-grace-a-la-tech.html', 'Présidentielle 2017 : Voxe veut réconcilier la jeunesse et la politique grâce à la tech - Politique - Numerama', 'http://www.numerama.com/content/uploads/2016/12/voxe-org.jpg', 'Faire évoluer le rapport à la vie politique grâce aux nouvelles technologies tout en intéressant les 18-35 ans à des élections dont ils se sentent souvent exclus : c\'est le double défi relevé par la startup Voxe. Rencontre avec cet acteur majeur de la Civic Tech française. À l’heure où 52 % des jeunes (entre 18 et 25 ans) comptent s’abstenir de voter à l’élection présidentiel', '2017-02-19 21:02:59', 0, NULL, NULL),
(55, 1, 'http://www.numerama.com/tech/218143-ce-que-mark-zuckerberg-a-appris-de-lia-en-developpant-la-sienne.html', 'Ce que Mark Zuckerberg a appris de l\'IA en développant la sienne - Tech - Numerama', 'http://www.numerama.com/content/uploads/2016/02/zuckerberg-1.jpg', 'Mark Zuckerberg a développé une intelligence artificielle pour contrôler sa maison. Qu\'a-t-il appris de cette expérience ?Souvenez-vous : début 2016, Mark Zuckerberg se donnait un défi personnel qui consistait à construire une « intelligence artificielle » capable de contrôler sa maison. Et contrairement à nous, simples mortels, qui ne sommes pas capables de tenir la moindre résolut', '2017-02-19 21:02:59', 0, NULL, NULL),
(56, 1, 'https://www.cath.ch/newsf/christine-pedotti-chretiens-de-gauche-avons-ete-naifs-paresseux/', 'Christine Pedotti: \"Nous, chrétiens de gauche, avons été naïfs et paresseux\" - cath.ch', 'https://www.cathkathcatt.ch/f/wp-content/uploads/sites/3/2016/12/CP.jpg', 'Christine Pedotti, écrivain, journaliste et directrice du mensuel \'Témoignage Chrétien\' (photo DR)\n								“Les cathos font leur primaire communion” titrait Libé pour décrypter le succès de François Fillon, suggérant l’uniformité du catholicisme français. Pour Christine Pedotti, écrivain, journaliste et directrice du mensuel Témoignage Chrétien, les mouvements conservateurs ne c', '2017-02-19 21:02:59', 0, NULL, NULL),
(57, 1, 'http://www.framboise314.fr/beetleblocks-comme-scratch-mais-en-3d/', 'BeetleBlocks : Comme Scratch, mais en 3D !', 'http://www.framboise314.fr/wp-content/uploads/2016/08/header_blog_framboise314_20160823.png', 'Scratch est bien connu, installé nativement sur le Raspberry Pi et très utile pour faire découvrir le code et les algorithmes aux débutants en programmation. Avec BeetleBlocks (les briques de la coccinelle ?) la programmation utilise le même système, mais au lieu de rester en deux dimensions, permet d’accéder à la 3D très simplement.\nBeetleBlocks est un environnement de  programmation ', '2017-02-19 21:02:59', 0, NULL, NULL),
(58, 1, 'http://m.slate.fr/story/130235/derive-malveillante-intelligence-artificielle-possible', 'Une dérive malveillante de l’intelligence artificielle est-elle possible?', 'http://www.slate.fr/sites/default/files/styles/500x229/public/3161505670_e0c235041a_o_2.jpg', 'Les progrès rapides de l’intelligence artificielle (IA) alimentent désormais un chaudron médiatique qui s’est emparé frénétiquement de ce sujet porteur, le plus souvent sans en maîtriser toute la complexité́. Les articles publiés ou mis en ligne évoquant l’IA ne se comptent plus dans la presse scientifique comme dans les médias généralistes et grand public.Pour autant, rares s', '2017-02-19 21:02:59', 0, NULL, NULL),
(59, 1, 'http://www.numerama.com/pop-culture/213528-la-cite-du-jeu-video-futur-espace-de-vulgarisation-scientifique.html', 'Pourquoi la future Cité du Jeu Vidéo nous fait rêver - Pop culture - Numerama', 'http://www.numerama.com/content/uploads/2016/12/imgp0168.jpg', 'La Cité des Sciences et de l\'Industrie peaufine l\'arrivée d\'une Cité du Jeu Vidéo dans ses locaux, un espace dédié à la recherche et à la vulgarisation scientifique fin 2017.Avec une exposition temporaire consacrée aux jeux vidéo en 2013, la Cité des Sciences et de l’Industrie avait pu prendre la température auprès du grand public pour l’élaboration de son prochain espace : la C', '2017-02-19 21:02:59', 0, NULL, NULL),
(60, 1, 'http://www.numerama.com/politique/213009-la-realite-augmentee-changera-notre-rapport-politique-au-monde-selon-snowden.html', 'Snowden : la réalité augmentée changera notre rapport politique au monde - Politique - Numerama', 'http://www.numerama.com/content/uploads/2016/11/13250398654_b0a1b50b0d_h.jpg', 'Bien qu\'Edward Snowden avoue ne pas jouer à Pokémon Go, le lanceur d\'alerte reste convaincu que la réalité augmentée jouera un rôle politique déterminant à l\'avenir. Mi-novembre, Edward Snowden donnait une nouvelle conférence à Oakland, en Californie, depuis son robot de téléconférence invité sur la scène du festival organisé par le site Fusion. La machine dotée d’un écran et', '2017-02-19 21:02:59', 0, NULL, NULL),
(61, 1, 'http://thierry-fayret.typepad.fr/accueil/2016/11/les-primaires-sont-elles-un-bien-pour-la-démocratie-.html', 'Les primaires sont-elles un bien pour la démocratie ?', 'http://thierry-fayret.typepad.fr/.a/6a00e54f9f737f883401b7c8b5bf07970b-600wi', 'Les primaires à gauche comme à droite sont globalement bien perçues. Leur taux de participation laisse à penser qu’elles sont plébiscitées par la population, en contraste avec des partis dont les effectifs ne font que décroître. S’agissant d’un vote de la population, elles apparaissent comme un acte de démocratie directe permettant de donner plus la parole à tous les citoyens. Tout', '2017-02-19 21:02:59', 0, NULL, NULL),
(62, 1, 'https://siecledigital.fr/2016/11/28/blockchain-ethereum-de-quoi-parle-t-on/', 'Blockchain Ethereum, de quoi parle-t-on ?', 'https://sd-cdn.fr/wp-content/uploads/2016/11/ethereum-overtakes-litecoin-in-market-cap-after-continued-upward-trend-300x167.jpg', 'partages       Dans mes premiers articles publiés ici même, vous avez pu comprendre globalement ce qu’est une blockchain, à quoi sert le minage dans le fonctionnement de celle-ci ou encore quels sont les enjeux du consensus dans le processus de cette technologie. Ces différents articles traitaient principalement de la blockchain Bitcoin, la première finalement à avoir vu le jour mais certa', '2017-02-19 21:02:59', 0, NULL, NULL),
(63, 1, 'http://www.lemagit.fr/conseil/Blockchain-quest-ce-quun-Smart-Contract-et-a-quoi-ca-sert', 'Blockchain : qu’est-ce qu’un Smart Contract et à quoi ça sert ?', 'http://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/Blockchain-2.jpg', 'Nick Szabo est un spécialiste du chiffrement et le créateur d’un précurseur de Bitcoin, appelé « Bit Gold ». Il est d’ailleurs soupçonné d’être le mystérieux inventeur de Bitcoin. Ce spécialiste a écrit pour la première fois sur les Smart Contracts en… 1994. Mais ce n’est que récemment que le concept commence à devenir réellement populaire. L’explication tient à la r', '2017-02-19 21:02:59', 0, NULL, NULL),
(64, 1, 'https://siecledigital.fr/2016/11/21/todoist-smart-schedule-intelligence-artificielle/', 'Todoist Smart Schedule : l\'intelligence artificielle au renfort de votre to-do.', 'https://sd-cdn.fr/wp-content/uploads/2016/11/Todoist-300x122.jpg', 'partages       L’application de gestion et de planification de tâches Todoist a renforcé son service avec un outil permettant de gérer sa to-do de façon intelligente : Smart Schedule.\nSi on a inventé les to-do, c’est pour ne jamais les terminer. C’est le constat que dresse l’éditeur qui nous annonce que 70% des utilisateurs de Todoist ont des tâches en retard. Effectivement … en o', '2017-02-19 21:02:59', 0, NULL, NULL),
(65, 1, 'http://linuxfr.org/news/trigger-happy-une-passerelle-entre-vos-services-internet', 'Trigger Happy - une passerelle entre vos services internet', 'https://sd-cdn.fr/wp-content/uploads/2016/11/Todoist-300x122.jpg', 'Trigger Happy se veut être une alternative libre du célébrissime site/service IFTTT. Le principe ? Récupérer des informations situées à l\'autre bout de la toile pour les publier/enregistrer ailleurs. Trigger Happy est disponible en version 1.1.0 depuis le 25 octobre dernier.\nExemples de scénarii :\nà chaque nouvelle dépêche sur LinuxFr.org, sans lever le petit doigt, je sauvegarde l\'art', '2017-02-19 21:02:59', 0, NULL, NULL),
(66, 1, 'https://siecledigital.fr/2016/10/19/intelligence-artificielle-microsoft/', 'L\'IA de Microsoft a atteint le niveau de compréhension de l\'Homme.', 'https://sd-cdn.fr/wp-content/uploads/2016/10/microsoft-copie-300x142.jpg', 'partages       C’est une très grande étape pour la recherche sur les intelligences artificielles. Les chercheurs de Microsoft ont réussi à créer une technologie qui comprend une conversation aussi bien qu’une personne normale.\nL’équipe d’ingénieurs de Microsoft Artificial Intelligence and Research a noté un taux d’erreur de seulement 5,9%. C’est approximativement égal au taux ', '2017-02-19 21:02:59', 0, NULL, NULL),
(67, 1, 'http://www.usine-digitale.fr/article/apres-le-krach-de-2008-les-algorithmes-nous-conduisent-ils-a-nouveau-au-bord-du-gouffre.N459962', 'Après le krach de 2008, les algorithmes nous conduisent-ils à nouveau au bord du gouffre ?', 'http://www.usine-digitale.fr/mediatheque/4/5/3/000353354_pageListeTypeACropped/big-data-medecine.jpg', '.  &#13;\n				&#13;\n				&#13;\n				&#13;\n					&#13;\n	Le titre de l’essai de Cathy O’Neil est génial avec son jeu de mots difficile à traduire. \"Weapons of math Destruction\" (*) signifie littéralement les armes de destruction mathématiques. Sauf qu’en anglais, \"maths\" se pronnonce \"mass\" et qu’elle signifie de cette façon les dangers que font courir les algorithmes omniprésents à nos s', '2017-02-19 21:02:59', 0, NULL, NULL),
(68, 1, 'http://www.liberation.fr/france/2016/11/02/fichier-des-pieces-d-identite-la-patronne-de-la-cnil-demande-un-debat-parlementaire_1525769', 'Fichier des pièces d\'identité : la patronne de la Cnil demande un débat parlementaire', 'http://md1.libe.com/photo/895393-la-presidente-de-la-cnil-isabelle-falque-pierrotin-a-paris-le-8-avril-2016.jpg?modified_at=1478109904&amp;picto=fb&amp;ratio_x=191&amp;ratio_y=100&amp;width=600', '«Il s’agit d’un changement majeur» : Isabelle Falque-Pierrotin, présidente de la Commission nationale de l’informatique et des libertés (Cnil) plaide pour un débat parlementaire sur le nouveau fichier regroupant les données personnelles de quasiment tous les Français, qui soulève des craintes pour les libertés publiques. «Il existait déjà un fichier pour les passeports de 15 mil', '2017-02-19 21:02:59', 0, NULL, NULL),
(69, 1, 'http://www.alterecoplus.fr/revenu-de-base-bonne-mauvaise-utopie/00012497', 'Revenu de base : la bonne et la mauvaise utopie', 'http://www.alterecoplus.fr/sites/default/files/public/styles/for_social_networks/public/field/image/istock_81585839_medium.jpg?itok=7fLfWQTg', 'Revenu de base : la bonne et la mauvaise utopie | AlterEco+ Alterecoplus&#13;\n&#13;\n    Aller au contenu principal&#13;\n&#13;\n &#13;\n \n  \n  &#13;\n    &#13;\n    &#13;\n    \n      \n      \n        \n          \n                    \n          \n          \n          \n            \n      \n      \n	    \n	\n	social	\n      \n        \n    \n      &#13;\n        &#13;\n        L\'idée d\'un revenu de base universel, inc', '2017-02-19 21:02:59', 0, NULL, NULL),
(70, 1, 'https://www.undernews.fr/hacking-hacktivisme/la-creature-de-frankenstein-reprend-vie-grace-a-liot.html', 'La Créature de Frankenstein reprend vie grâce à l’IoT', 'https://www.undernews.fr/wp-content/uploads/2016/10/ddos-norse-150x150.jpg', '2 novembre 2016 - 					Aucun commentaire					- Temps de lecture : 5 minute(s) 					- Classé dans : Hacking Publié par UnderNews ActuSuite à une attaque DDoS (déni de service distribué), menée contre le service informatique DynDNS, un grand nombre de sites Internet ont été inaccessibles le 21 octobre dans la zone outre-Atlantique. Les échos se sont ressentis jusqu’en Europe et le knock-o', '2017-02-19 21:02:59', 0, NULL, NULL),
(71, 1, 'http://www.usine-digitale.fr/article/interview-pourquoi-waze-echange-des-donnees-avec-les-villes.N457832', '[Interview] Pourquoi Waze échange des données avec les villes', 'http://www.usine-digitale.fr/mediatheque/7/9/9/000487997_pageListeTypeACropped/waze-appli-accident.jpg', '[Interview] Pourquoi Waze échange des données avec les villes © Waze&#13;\n			&#13;\n										&#13;\n																									&#13;\n				&#13;\n				&#13;\n				&#13;\n					&#13;\n	L\'Usine Digitale – En quoi consiste le programme \"Connected Citizens\" de Waze ?&#13;\n&#13;\n	Paige Fitzgerald – Il s\'agit d\'un programme d\'échange gratuit de données anonymisées lancé en octobre 2014. Nous avions dix p', '2017-02-19 21:02:59', 0, NULL, NULL),
(72, 1, 'https://www.franceculture.fr/emissions/affaires-etrangeres/comment-la-cyberguerre-aura-lieu', 'Comment la cyberguerre aura lieu', 'https://s3-eu-west-1.amazonaws.com/cruiser-production/2016/10/c8e93015-cd41-4c2c-8621-c65b8d19a82c/600x337_maxstockworld349788.jpg', 'Fermer X\nEn poursuivant votre navigation sur le site, vous acceptez l’utilisation des cookies pour vous proposer des contenus et services adaptés à vos centres d’intérêt.\nEn savoir plus et gérer ces paramètres.', '2017-02-19 21:02:59', 0, NULL, NULL);
INSERT INTO `content` (`id`, `source_id`, `url`, `title`, `image_preview`, `content`, `date_import`, `archived`, `date_cure`, `vedette`) VALUES
(73, 1, 'http://roget.biz/un-digest-de-mes-tops-applications-sur-android-2016', 'tops applications sur Android 2016', 'https://s3-eu-west-1.amazonaws.com/cruiser-production/2016/10/c8e93015-cd41-4c2c-8621-c65b8d19a82c/600x337_maxstockworld349788.jpg', 'Un digest de mes tops applications sur Android 2016&#13;\n25/10/2016 ( blogging )    Thierry Roget 2 avis &#13;\n&#13;\n&#13;\n&#13;\n&#13;\n&#13;\nTous les ans, j’établis une liste des applications qui sont sur mon (mes) mobiles, ça me permet de voir comment évolue le web et aussi comment  j’évolue. J’avoue que la liste ne change pas vraiment d’une année à l’autre, mais il y a tout de m', '2017-02-19 21:02:59', 0, NULL, NULL),
(74, 1, 'http://www.developpez.com/actu/105447/Fragmentation-de-l-open-source-faut-il-se-concentrer-sur-un-ou-deux-projets-viables-ou-multiplier-les-alternatives-pour-la-competitivite/', 'Page introuvable', 'https://s3-eu-west-1.amazonaws.com/cruiser-production/2016/10/c8e93015-cd41-4c2c-8621-c65b8d19a82c/600x337_maxstockworld349788.jpg', 'wallabag can\'t retrieve contents for this article. Please report this issue to us.', '2017-02-19 21:02:59', 0, NULL, NULL),
(75, 1, 'http://www.atout-dsi.com/le-leadership-partage-dsi-startup-accelerateur-de-transformation-numerique/', 'Leadership partagé DSI-Startup: accélérateur de transfo numérique | AtoutDSI, pour les DSI qui se transforment', 'http://www.atout-dsi.com/wp-content/uploads/2016/09/ANTHONY-HIE-INSTITUT-CATHOLIQUE-DE-PARIS.jpg', 'La transformation numérique appelle de nouvelles approches managériales et organisationnelles, comme le leadership partagé. Anthony Hié, Directeur des SI &amp; du Numérique (DSIN) de l’Institut Catholique de Paris, signe ici un avis d’expert sur sa démarche de leadership partagé entre DSI et startup. Son contexte : il collabore actuellement avec deux startups (eLamp, le premier RCE - ', '2017-02-19 21:02:59', 0, NULL, NULL),
(76, 1, 'http://www.itforbusiness.fr/component/k2/item/8217-l', 'L\'IoT doit s\'appuyer sur le Libre', 'http://www.atout-dsi.com/wp-content/uploads/2016/09/ANTHONY-HIE-INSTITUT-CATHOLIQUE-DE-PARIS.jpg', '&#13;\n	Tout comme l\'Internet a bouleversé notre quotidien au siècle dernier, la révolution de l\'intégration d\'objets physiques dans le réseau des réseaux est en marche. On peut raisonnablement se demander s\'il s\'agit  réellement d\'une révolution ou tout simplement de l\'exposition d\'un ensemble de technologies autrefois ignorées du grand public (voire de certains industriels). De nombreu', '2017-02-19 21:02:59', 0, NULL, NULL),
(77, 1, 'http://www.numerama.com/sciences/201926-se-cache-derriere-asgardia-premiere-nation-de-lespace.html', 'Qu\'est-ce qui se cache derrière Asgardia, la première nation de l\'espace ? - Sciences - Numerama', 'http://www.numerama.com/content/uploads/2016/10/asgardia-space-nation-website-logo.jpg', 'La première nation de l\'espace est en train de se constituer progressivement ici sur Terre. Le projet semble presque faire partie d\'un roman de science-fiction mais ses pères fondateurs sont vraiment sûrs de pouvoir le réaliser. Une nouvelle nation, appelée Asgardia, a été fondée récemment et compte déjà plus de 370 000 citoyens. Cependant, elle ne se trouve pas sur notre planète, mais', '2017-02-19 21:02:59', 0, NULL, NULL),
(78, 1, 'http://mobile.lesinrocks.com/2016/10/11/actualite/nicolas-sarkozy-fin-de-lhistoire-11871083/', 'Les Inrocks - Edito: Nicolas Sarkozy ne sera plus jamais président de la République', 'http://statics.lesinrocks.com/content/thumbnails/uploads/2016/10/france-vote-primaries-republicains-tt-width-1200-height-630-fill-0-crop-1-bgcolor-000000-nozoom_default-1-lazyload-0.jpg', 'Dimanche, fin d’après-midi, sur LCI. Nicolas Sarkozy est au Zénith, façon de parler, vraiment. “Il entend donner un coup de fouet à sa campagne”, peut-on lire dans un “liner” sous l’image du petit homme qui bouge ses bras et fait ses simagrées d’épaules. Rires enregistrés. Derrière lui, on a mis des jeunes, dont un sosie de Nana Mouskouri sur la droite.\nFace à lui, un part', '2017-02-19 21:02:59', 0, NULL, NULL),
(79, 1, 'http://www.lagazettedescommunes.com/465258/les-civics-tech-peuvent-reconnecter-citoyens-et-systeme-politique-axelle-lemaire/', 'No title found', 'http://www.lagazettedescommunes.com/wp-content/uploads/2016/10/lemaire-axelle-photo-mein-patrick-vedrune.jpg', 'L’enquête portant sur la « Démocratie participative et le Numérique » met en évidence le fait que les élus de petites et moyennes collectivités ne sont pas encore convaincues par les outils numériques. Comment peut-on expliquer ce blocage ?\nJe ne sais pas si c’est un blocage ou une méconnaissance. Il y a une conscience chez les élus du défi qui consiste à restaurer la confiance da', '2017-02-19 21:02:59', 0, NULL, NULL),
(80, 1, 'http://www.europe1.fr/politique/la-femme-voilee-daujourdhui-sera-la-marianne-de-demain-ce-qua-voulu-dire-francois-hollande-2870425', '\"La femme voilée d\'aujourd\'hui sera la Marianne de demain\" : ce qu\'a voulu dire Hollande', 'http://cdn1-europe1.new2.ladmedia.fr/var/europe1/storage/images/europe1/politique/la-femme-voilee-daujourdhui-sera-la-marianne-de-demain-ce-qua-voulu-dire-francois-hollande-2870425/29303680-3-fre-FR/L', '\"Marianne, elle a le sein nu, parce qu\'elle nourrit le peuple. Elle n\'est pas voilée, parce qu\'elle est libre\", affirmait fin août Manuel Valls. Une sortie qui, en pleine polémique sur le burkini, n’avait pas manqué de faire réagir. Un mois et demi plus tard, c’est au tour de François Hollande d’invoquer, après son Premier ministre, l’un des symboles de la République. Là encore, ?', '2017-02-19 21:03:00', 0, NULL, NULL),
(81, 1, 'http://rue89.nouvelobs.com/rue89-culture/2016/10/09/droles-superstitions-autour-smartphones-263578?_escaped_fragment_=', 'Ces drôles de superstitions autour de nos smartphones - Rue89 - L\'Obs', 'http://api.rue89.nouvelobs.com/sites/news/files/styles/mobile2-tablette-asset-center/public/assets/image/2016/03/electronic_email.preview.jpg', 'Laurent, 27 ans, est un jeune homme tout à fait rationnel. Mais un pan de lui insoupçonné se révèle quand il parle de son téléphone : «  Il a une présence dans la pièce. Comme un être humain. Comme un bébé. Parfois je le cache sous du linge sale pour oublier où il se trouve. D’ailleurs, je l’éteins avant d’avoir une relation sexuelle.  »\n\nMatthias, la trentaine, confie?', '2017-02-19 21:03:00', 0, NULL, NULL),
(82, 1, 'http://www.frandroid.com/marques/samsung/381623_zoom-capteur-diris-samsung-galaxy-note-7', 'Zoom sur le capteur d\'iris du Samsung Galaxy Note 7 - FrAndroid', 'http://images.frandroid.com/wp-content/uploads/2016/10/galaxynote7_feature_iris_main_final_1.jpg', 'Bien qu’on parle actuellement du Galaxy Note 7 pour sa propension à exploser un peu trop spontanément, le téléphone de Samsung propose également de nombreuses technologies inédites dans le monde des smartphones Android, dont son scanner d’iris, sur lequel nous nous concentrons aujourd’hui.\n\nSi la majorité des appareils mobiles haut de gamme bénéficient désormais de lecteur d’emp', '2017-02-19 21:03:00', 0, NULL, NULL),
(83, 1, 'https://www.cfdt.fr/portail/theme/formation-syndicale/spoc-les-cles-de-la-mds-participez-au-cours-en-ligne-sur-l-evolution-du-dialogue-social-srv2_384739', 'SPOC \"Les clés de la MDS\" : participez au cours en ligne sur l\'évolution du dialogue social !', 'http://images.frandroid.com/wp-content/uploads/2016/10/galaxynote7_feature_iris_main_final_1.jpg', '&#13;\n	 		publié le  29/09/2016  à 16H36&#13;\n			 &#13;\n				par&#13;\n			eFormation&#13;\n			&#13;\n	 	\n		\n		\n		  \n		    \n			            	\n	  	\n	  	\n	  	  \n Le 17 octobre 2016, la CFDT lance son premier cours en ligne ouvert à tous ses élus d\'entreprise : \"Les clés de la MDS\". Ce SPOC (cours en ligne privé) s\'étale sur quatre semaines. Les participants suivront un module chaque semaine : l\'év', '2017-02-19 21:03:00', 0, NULL, NULL),
(84, 1, 'http://www.netpublic.fr/2016/10/78-applications-mobiles-pour-apprendre-et-creer-avec-le-numerique/', 'NetPublic » 78 applications mobiles pour apprendre et créer avec le numérique', 'http://images.frandroid.com/wp-content/uploads/2016/10/galaxynote7_feature_iris_main_final_1.jpg', 'Comment ne pas s’y perdre dans les applis mobiles pour apprendre et créer avec des tablettes et des smartphones ? Comment choisir parmi des milliers d’applications celles qui conviennent le plus à votre projet avec des enfants et des adolescents (mais aussi pour tous) ?\nLa Délégation Académique au numérique éducatif de l’Académie de Versailles propose sur le site Apps Listes, 8 lis', '2017-02-19 21:03:00', 0, NULL, NULL),
(85, 1, 'http://www.tropheedesce.com/', 'Trophée des CE', 'http://images.frandroid.com/wp-content/uploads/2016/10/galaxynote7_feature_iris_main_final_1.jpg', '« Le CE est une réelle structure économique et sociale qui participe activement au bien-être des salariés et à la réussite de l’entreprise »\nPatrice Thiry PDG de ProwebCE, créateur du Trophée des CE\nDepuis 70 ans le rôle du Comité d’Entreprise ne cesse d’évoluer. Grâce aux CE, les salariés français ainsi que leurs familles bénéficient de nombreux avantages sur le plan soc', '2017-02-19 21:03:00', 0, NULL, NULL),
(86, 1, 'http://www.frandroid.com/produits-android/realite-virtuelle/352093_6-choses-a-faire-samsung-gear-vr', '7 choses vraiment intéressantes à faire avec un Samsung Gear VR - FrAndroid', 'http://images.frandroid.com/wp-content/uploads/2016/04/samsung-gear-vr-1-sur-1.jpg', 'HTC et Oculus ne sont pas les seuls à proposer un casque de réalité virtuelle. À condition de posséder un Galaxy S6 ou S7, Samsung dispose également de son propre casque, le Gear VR, qui affiche actuellement le meilleur rapport qualité-prix pour ce qui est de la réalité virtuelle. Et ce n’est pas parce qu’il coûte moins de 100 euros que c’est un appareil au rabais. Voici sept cho', '2017-02-19 21:03:00', 0, NULL, NULL),
(87, 1, 'http://www.slate.fr/story/124142/femmes-voilees-coercition-pressions', 'Les femmes musulmanes sont-elles forcées à porter le voile, comme on l\'entend dire?', 'http://www.slate.fr/sites/default/files/styles/1090x500/public/femme_voile_baguette.jpg', 'De nombreux hommes politiques affirment ou suggèrent que la plupart des femmes voilées subissent des pressions et sont contraintes de porter le foulard, comme Manuel Valls, dans un tribune intitulée «En France, les femmes sont libres». Notre enquête démontre que ces faits sont très minoritaires. \n      \n      Début septembre, dans une tribune intitulée «En France, les femmes sont libres', '2017-02-19 21:03:00', 0, NULL, NULL),
(88, 1, 'http://m.nouvelobs.com/rue89/sur-le-radar/20170223.OBS5690/que-se-passe-t-il-dans-un-cerveau-amoureux.html?xtref=http://m.nouvelobs.com/rue89/', 'Que se passe-t-il dans un cerveau amoureux ?', 'http://referentiel.nouvelobs.com/file/15922569-que-se-passe-t-il-dans-un-cerveau-amoureux.jpg', 'Serge Stoléru, psychiatre et chercheur à l’Inserm, a utilisé la neuro-imagerie fonctionnelle pour comprendre et théoriser les bases neurologiques du désir sexuel et de l’amour.Dans son livre \"Un cerveau nommé désir\" (éditions Odile Jacob), il résume 20 ans de recherches et montre les processus en œuvre dans le cerveau \"pour les connaître et, le cas échéant, ne pas se laisser dépa', '2017-03-04 09:35:49', 0, NULL, NULL),
(89, 1, 'http://www.argusdelassurance.com/acteurs/allemagne-allianz-toujours-a-l-affut-d-une-acquisition.115857', 'Allemagne : Allianz toujours à l’affût d’une acquisition', 'http://www.argusdelassurance.com/mediatheque/8/1/7/000032718_5.jpg', '&#13;\n	Oliver Bäte n’a pas pu réaliser son premier rêve à la tête du numéro 1 de l’assurance en Europe : celle d’une grande acquisition pour développer le groupe à l’international. Quelques heures avant la conférence de presse sur les résultats 2016, vendredi 17 février, Allianz a annoncé un programme de rachat d’actions allant jusqu’à 3 Md€ ainsi qu\'une augmentation du ', '2017-03-04 09:35:49', 0, NULL, NULL),
(90, 1, 'http://acteursdeleconomie.latribune.fr/debats/grands-entretiens/2016-02-11/edgar-morin-le-temps-est-venu-de-changer-de-civilisation.html', 'Edgar Morin : \"Le temps est venu de changer de civilisation\"', 'http://static.latribune.fr/full_width/550247/edgar-morin.jpg', 'Acteurs de l\'économie - La Tribune. Attentats à Paris, état d\'urgence, rayonnement du Front National, vague massive de migration, situation économique et sociale déliquescente symbolisée par un taux de chômage inédit (10,2 % de la population) : la France traverse une époque particulièrement inquiétante. La juxtaposition de ces événements révèle des racines et des manifestations comm', '2017-03-04 09:35:49', 0, NULL, NULL),
(91, 1, 'http://www.lyonne.fr/auxerre/economie/emploi/2017/02/01/macron-la-nouvelle-classe-creative-pour-le-sociologue-jean-viard_12265612.html', 'Interview - Macron, « la nouvelle classe créative » pour le sociologue Jean Viard', 'http://www.lyonne.fr/photoSRC/bq1SUelNbWelbDCxbBjzfRMlEk0idrKIebrWVSMVgfq8f_p0HkyGO6biia076OQl6VTcz5L970tkHFBgX3JrMZlHFBdTzjXGcNLPP6P5r_/conference-sociologue-jean-viard-politique-la-scene-des-idee_30', 'Sociologue, directeur de recherches CNRS au Cevipof, Jean Viard est l’un des plus grands spécialistes français de la question du temps. Son dernier ouvrage s’intitule Le moment est venu de penser à l’avenir.\n																			Selon vous, « la politique se déstructure ». Pourquoi ? Depuis la Révolution industrielle, on a une culture politique bâtie sur les différences de classes', '2017-03-04 09:35:49', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `letter`
--

CREATE TABLE `letter` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `letter`
--

INSERT INTO `letter` (`id`, `title`, `introduction`, `user_id`) VALUES
(1, 'azerty', NULL, 1),
(2, 'monTitre', 'monIntro', 1);

-- --------------------------------------------------------

--
-- Structure de la table `lettercontent`
--

CREATE TABLE `lettercontent` (
  `id` int(10) UNSIGNED NOT NULL,
  `letter_id` int(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `lettercontent`
--

INSERT INTO `lettercontent` (`id`, `letter_id`, `content_id`, `introduction`) VALUES
(1, 1, 4, 'intro1 pour tester'),
(2, 1, 5, NULL),
(3, 1, 6, NULL),
(4, 1, 13, NULL),
(6, 2, 7, NULL),
(7, 2, 8, NULL),
(8, 2, 10, NULL),
(9, 2, 12, NULL),
(10, 2, 16, NULL),
(11, 2, 17, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `source`
--

CREATE TABLE `source` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `lien` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `source`
--

INSERT INTO `source` (`id`, `type`, `lien`, `user_id`) VALUES
(1, 1, 'wallabag', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'Identifiant',
  `login` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `login`, `username`, `password`) VALUES
(1, 'gwen', 'gwenlune', 'azerty'),
(2, 'magali', 'magaliluneau', 'uytyteuei');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Index pour la table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `source_id` (`source_id`);

--
-- Index pour la table `letter`
--
ALTER TABLE `letter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `lettercontent`
--
ALTER TABLE `lettercontent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `letter_id` (`letter_id`),
  ADD KEY `content_id` (`content_id`);

--
-- Index pour la table `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT pour la table `letter`
--
ALTER TABLE `letter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `lettercontent`
--
ALTER TABLE `lettercontent`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `source`
--
ALTER TABLE `source`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifiant', AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`);

--
-- Contraintes pour la table `letter`
--
ALTER TABLE `letter`
  ADD CONSTRAINT `letter_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `lettercontent`
--
ALTER TABLE `lettercontent`
  ADD CONSTRAINT `lettercontent_ibfk_1` FOREIGN KEY (`letter_id`) REFERENCES `letter` (`id`),
  ADD CONSTRAINT `lettercontent_ibfk_2` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`);

--
-- Contraintes pour la table `source`
--
ALTER TABLE `source`
  ADD CONSTRAINT `source_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
