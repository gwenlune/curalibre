<?php

class LetterContent_model extends CI_Model {

    public function getLetterContent($id = NULL) {
        if (!isset($id)) {
            $query = $this->db->get('lettercontent');
            return $query->result_array();
        }

        $query = $this->db->get_where('lettercontent', array('id' => $id));
        return $query->row_array();
    }

    public function getLetterContentByLetterId($letter_id) {
        $query = $this->db->get_where('lettercontent', array('letter_id' => $letter_id));
        return $query->result_array();
    }
    
    
    public function setLetterContent($data) {

        $dataenvoyee = array(
            'letter_id' => $data['letter_id'],
            'content_id' => $data['content_id'],
            'introduction' => $data['introduction']
        );

        return $this->db->insert('lettercontent', $dataenvoyee);
    }

}
