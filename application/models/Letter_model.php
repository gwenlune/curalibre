<?php

class Letter_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('LetterContent_model');
    }

    public function getLetter($id) {

        $query = $this->db->get_where('letter', array('id' => $id));
        $row = $query->row_array();
        if (isset($row)) {
            $letter['id'] = $row["id"];
            $letter['title'] = $row["title"];
            $letter['introduction'] = $row["introduction"];
            $letter['user_id'] = $row["user_id"];
            $letter['items'] = $this->LetterContent_model->getLetterContentByLetterId($id);
            return $letter;
        } else {
            return NULL;
        }
    }

    public function setLetter($data) {

        $dataenvoyee = array(
            'title' => $data['title'],
            'introduction' => $data['introduction'],
            'user_id' => $data['user_id']
        );
        $result = $this->db->insert('letter', $dataenvoyee);
        $id = $this->db->insert_id();
        foreach ($data['items'] as $item) {
            $this->LetterContent_model->setLetterContent($item);
        }
        return $result;
    }

    public function set_letter() {

        $dataenvoyee = array(
            'title' => $this->input->post('title'),
            'introduction' => $this->input->post('introduction'),
            'user_id' => $this->input->post('user_id')
        );
        $result = $this->db->insert('letter', $dataenvoyee);
        $id = $this->db->insert_id();

        $items = $this->input->post(NULL, FALSE);
        foreach ($items as $index => $valeur) {
            if ($index != 'title' && $index != 'introduction' && $index != 'user_id' && $index != 'submit') {
                $item = [
                    'letter_id' => $id,
                    'content_id' => $index,
                    'introduction' => NULL
                ];
                //echo '- ' . $index . ' : ' . $valeur . '<br/>';
                $this->LetterContent_model->setLetterContent($item);
            }
        }

    }

    public function getLetterByUserID($user_id) {
        $query = $this->db->get_where('letter', array('user_id' => $user_id));
        return $query->result_array();
    }
}
