<?php

class User_model extends CI_Model {

    public function get_user($id = NULL) {
        if (!isset($id)) {
            $query = $this->db->get('user');
            return $query->result_array();
        }

        $query = $this->db->get_where('user', array('id' => $id));
        return $query->row_array();
    }

    public function get_user_bylogin($login) {
        $query = $this->db->get_where('user', array('login' => $login));
        return $query->row_array();
    }

    public function set_user() {

        $data = array(
            'username' => $this->input->post('username'),
            'login' => $this->input->post('login'),
            'password' => $this->input->post('password')
        );

        return $this->db->insert('user', $data);
    }

}
