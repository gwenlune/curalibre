<?php

class Content_model extends CI_Model {

    public function getContent($id = NULL) {
        if (!isset($id)) {
            $query = $this->db->get('content');
            return $query->result_array();
        }

        $query = $this->db->get_where('content', array('id' => $id));
        return $query->row_array();
    }

    public function setContent($data, $source_id = 1) {
        $date = new DateTime();
        $dataenvoyee = array(
            'source_id' => $source_id,
            'url' => $data['url'],
            'title' => $data['title'],
            'content' => $data['content'],
            'archived' => 0
        );
        if (isset($data["preview_picture"])) {
            $dataenvoyee['image_preview'] = $data['preview_picture'];
        }

        return $this->db->insert('content', $dataenvoyee);
    }

    public function isContentExists($url, $source_id = 1) {
        $query = $this->db->get_where('content', array('source_id' => $source_id, 'url' => $url));
        $ret = $query->row_array();
        if (empty($ret)) {
            return 0;
        } else {
            return $ret['id'];
        }
    }
    
    public function getContentBySource($source_id = 1,$archived = 0) {
        $query = $this->db->get_where('content', array('source_id' => $source_id, 'archived' => $archived));
        return $query->result_array();
    }

}
