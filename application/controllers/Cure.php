<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cure extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Content_model');
        $this->load->model('Letter_model');
        $this->load->model('LetterContent_model');
        $this->load->helper('url');
    }

    public function accueil() {
        echo 'Hello World!';
    }

    public function info() {
        phpinfo();
    }

    public function lireWallabag() {

        $data = array(
            'grant_type' => 'password',
            'client_id' => '1_2fsl2y81wgysow8wk0gowgcwo0s0wgs8g4wwkog0g4kskw8s48',
            'client_secret' => '3tkblyie81s0k84wkgswgc40kowoo8ss84kgskoc4ws40g0c88',
            'username' => 'gwenlune',
            'password' => 'GwenQuiTape'
        );

        $postdata = http_build_query($data);

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $result = file_get_contents('http://wallabag.home.monsiteinternet.org/oauth/v2/token', false, $context);

        $obj = json_decode($result);
        $access_token = $obj->{'access_token'};

        $newurl = 'http://wallabag.home.monsiteinternet.org/api/entries.json?access_token=' . $access_token . '&perPage=1';
        $opts = array('http' =>
            array(
                'method' => 'GET'
            )
        );
        $result = file_get_contents($newurl, false, stream_context_create($opts));
        $obj = json_decode($result);
        $nbitem = $obj->{'total'};
        $newurl = 'http://wallabag.home.monsiteinternet.org/api/entries.json?access_token=' . $access_token . '&perPage=' . $nbitem;
        $result = file_get_contents($newurl, false, stream_context_create($opts));
        $obj = json_decode($result, true);

        $this->load->view('template/header');

        for ($i = 0; $i < $nbitem; $i++) {
            $lelien = $obj["_embedded"]["items"][$i];
            $data['id'] = $lelien["id"];
            $data['url'] = $lelien["url"];
            $data['content'] = substr(trim(strip_tags($lelien["content"])), 0, 400);
            $data['title'] = $lelien["title"];
            if (isset($lelien["preview_picture"])) {
                $data['preview_picture'] = $lelien["preview_picture"];
            }
            $ret = $this->Content_model->isContentExists($data['url']);
            if ($ret == 0) {
                $this->Content_model->setContent($data);
                $this->load->view('content/wallItemAddView', $data);
            } else {
                $this->load->view('content/wallItemNotNewView', $data);
            }
            //$this->load->view('wallItemView', $data);
        }
        $this->load->view('template/footer');
        //var_dump($obj,true);
    }

    public function contentList() {
        $this->load->view('template/header');
        $data = $this->Content_model->getContentBySource();
        foreach ($data as $item) {
            $this->load->view('content/contentItemView', $item);
        }
        $this->load->view('template/footer');
    }

    public function contentSelectList() {
        $this->load->helper('form');
        $this->load->view('template/header');
        $this->load->view('content/contentBeginSelectView');
        $data = $this->Content_model->getContentBySource();
        foreach ($data as $item) {
            $this->load->view('content/contentItemSelectView', $item);
        }
        $this->load->view('content/contentEndSelectView');
        $this->load->view('template/footer');
    }

    public function contentCreateNewPage() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Titre', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->contentSelectList();
        } else {
            $this->Letter_model->set_Letter();
            $this->load->view('letter/createsuccess');
        }
    }

    public function letterContentList($id) {
        $this->load->view('template/header');
        $data = $this->Letter_model->getLetter($id);
        $this->load->view('letter/headerLetter', $data);
        foreach ($data['items'] as $item) {
            $lc = $this->Content_model->getContent($item['content_id']);
            $lc['introduction'] = $item['introduction'];
            $this->load->view('letter/contentItemView', $lc);
        }
        $this->load->view('template/footer');
    }

    public function letterList($user_id) {
        $this->load->view('template/header');
        $data = $this->Letter_model->getLetterByUserId($user_id);
        $this->load->view('letter/headerLetterList', $data);
        foreach ($data as $item) {
            $this->load->view('letter/letterList', $item);
        }
        $this->load->view('template/footer');
    }

}
