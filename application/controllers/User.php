<?php

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        //$this->load->helper('url_helper');
    }

    public function index() {
        $data['user'] = $this->User_model->get_user();
        $this->load->view('template/header', $data);
        $this->load->view('user/index', $data);
        $this->load->view('template/footer');
    }

    public function view($id) {
        $data['user_item'] = $this->User_model->get_user($id);

        if (empty($data['user_item'])) {
            show_404();
        }

        //$data['title'] = $data['news_item']['title'];

        $this->load->view('template/header', $data);
        $this->load->view('user/view', $data);
        $this->load->view('template/footer');
    }

    public function create() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('login', 'login', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('template/header');
            $this->load->view('user/create');
            $this->load->view('template/footer');
        } else {
            $this->User_model->set_user();
            $this->load->view('user/success');
        }
    }

}
