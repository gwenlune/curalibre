<div class="contenu">
    <?php if (isset($introduction)) { ?>
    <p>Intro <?php echo $introduction; ?></p>
    <?php } ?>
    
    <p><?php echo $id; ?> -> <a href="<?php echo $url; ?>"><?php echo $title; ?></a></p>

    <?php if (isset($image_preview)) { ?>
        <img src="<?php echo $image_preview; ?>" width=250 alt=""/>
    <?php } ?>
    <div>
        <?php echo $content; ?>
    </div>
</div>
